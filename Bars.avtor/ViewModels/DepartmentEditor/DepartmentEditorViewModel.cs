namespace Bars.avtor
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Отдел'
    /// </summary>
    public interface IDepartmentEditorViewModel : IEntityEditorViewModel<Bars.avtor.Department, DepartmentEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Отдел'
    /// </summary>
    public interface IDepartmentEditorHandler : IEntityEditorViewModelHandler<Bars.avtor.Department, DepartmentEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Отдел'
    /// </summary>
    public abstract class AbstractDepartmentEditorHandler : EntityEditorViewModelHandler<Bars.avtor.Department, DepartmentEditorModel>, IDepartmentEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Отдел'
    /// </summary>
    public class DepartmentEditorViewModel : BaseEditorViewModel<Bars.avtor.Department, DepartmentEditorModel>, IDepartmentEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override DepartmentEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new DepartmentEditorModel();
            var varCompanyId = @params.Params.GetAs<long>("Company_Id", 0);
            if (varCompanyId > 0)
            {
                var queryCompany = Container.ResolveDomain<Bars.avtor.Company>().GetAll().Where(x => x.Id == varCompanyId);
                model.Company = queryCompany.Select(x => new DepartmentEditorCompanyControlModel{Id = x.Id, Name = x.Name, }).FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Отдел' в модель представления
        /// </summary>
        protected override DepartmentEditorModel MapEntityInternal(Bars.avtor.Department entity)
        {
            // создаем экзепляр модели
            var model = new DepartmentEditorModel();
            model.Id = entity.Id;
            model.Name = ((System.String)(entity.Name));
            model.Company = entity.Company == null ? null : new DepartmentEditorCompanyControlModel{Id = (System.Int64? )(entity.Return(p => p.Company).Return(p => p.Id)), Name = entity.Return(p => p.Company).Return(p => p.Name), };
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Отдел' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.avtor.Department entity, DepartmentEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Name = model.Name;
            entity.Company = TryLoadEntityById<Bars.avtor.Company>(model.Company?.Id);
        }
    }
}