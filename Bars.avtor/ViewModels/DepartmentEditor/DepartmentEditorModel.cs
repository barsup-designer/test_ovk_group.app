namespace Bars.avtor
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Отдел' для отдачи на клиент
    /// </summary>
    public class DepartmentEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public DepartmentEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("23dfdd70-4528-4d0f-9ffc-b88ec5be0eee")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Компания
        /// </summary>
        [Bars.B4.Utils.Display("Компания")]
        [Bars.Rms.Core.Attributes.Uid("78335e40-ac46-4954-82f5-e4ae7de3bab7")]
        public virtual Bars.avtor.DepartmentEditorCompanyControlModel Company
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Компания'
    /// </summary>
    public class DepartmentEditorCompanyControlModel
    {
        /// <summary>
        /// Свойство 'Отдел.Компания.Идентификатор'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Отдел.Компания.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Отдел.Компания.Наименование'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Отдел.Компания.Наименование")]
        public System.String Name
        {
            get;
            set;
        }
    }
}