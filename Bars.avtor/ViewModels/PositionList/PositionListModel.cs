namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Должность'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Должность")]
    public class PositionListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Наименование' (псевдоним: Name)
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("b241b741-5ac9-4af7-bf0d-8d2429ad8a20")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Отдел.Наименование' (псевдоним: Department_Name)
        /// </summary>
        [Bars.B4.Utils.Display("Отдел.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("6ff204aa-75a2-4ea5-9f9a-4cdb5861e484")]
        public virtual System.String Department_Name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Компания.Наименование' (псевдоним: Company_Name)
        /// </summary>
        [Bars.B4.Utils.Display("Компания.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("16d436a1-c33e-496f-87bc-1923fe189660")]
        public virtual System.String Company_Name
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}