namespace Bars.avtor
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Должность'
    /// </summary>
    public interface IPositionListQuery : IQueryOperation<Bars.avtor.Position, Bars.avtor.PositionListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Должность'
    /// </summary>
    public interface IPositionListQueryFilter : IQueryOperationFilter<Bars.avtor.Position, Bars.avtor.PositionListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Должность'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Должность")]
    [ApiAccessible("Реестр Должность")]
    public class PositionListQuery : RmsEntityQueryOperation<Bars.avtor.Position, Bars.avtor.PositionListModel>, IPositionListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "PositionListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public PositionListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.avtor.Position> Filter(IQueryable<Bars.avtor.Position> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.avtor.PositionListModel> Map(IQueryable<Bars.avtor.Position> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.avtor.Position>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.avtor.PositionListModel{Id = x.Id, _TypeUid = "6fe4c6e0-2fdf-477a-bf72-8ba97ae13f0e", Name = ((System.String)(x.Name)), Department_Name = ((System.String)(x.Department.Name)), Company_Name = ((System.String)(x.Company.Name)), });
        }
    }
}