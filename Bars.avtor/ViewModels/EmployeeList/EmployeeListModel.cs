namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Модель записи реестра 'Реестр Сотрудник'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Сотрудник")]
    public class EmployeeListModel
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор")]
        [Bars.Rms.Core.Attributes.Uid("Bars.B4.DataAccess.PersistentObject, Bars.B4.Core/Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Идентификатор типа сущности
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор типа сущности")]
        [Bars.Rms.Core.Attributes.Uid("_TypeUid")]
        public virtual System.String _TypeUid
        {
            get;
            set;
        }

        /// <summary>
        /// Наименование редактора сущности
        /// </summary>
        [Bars.B4.Utils.Display("Наименование редактора сущности")]
        [Bars.Rms.Core.Attributes.Uid("_EditorName")]
        public virtual System.String _EditorName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Фамилия' (псевдоним: LastName)
        /// </summary>
        [Bars.B4.Utils.Display("Фамилия")]
        [Bars.Rms.Core.Attributes.Uid("71d45fa8-20a7-4241-9b21-f2704a14cf85")]
        public virtual System.String LastName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Имя' (псевдоним: FirestName)
        /// </summary>
        [Bars.B4.Utils.Display("Имя")]
        [Bars.Rms.Core.Attributes.Uid("483d7fd2-17a9-478a-b328-e618f179a69b")]
        public virtual System.String FirestName
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Отчество' (псевдоним: Patronymic)
        /// </summary>
        [Bars.B4.Utils.Display("Отчество")]
        [Bars.Rms.Core.Attributes.Uid("3c378657-ea89-4a54-8dc3-34d0d24e6410")]
        public virtual System.String Patronymic
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Должность.Компания.Наименование' (псевдоним: Position_Company_Name)
        /// </summary>
        [Bars.B4.Utils.Display("Должность.Компания.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("8a84f546-9587-4449-9862-7ec2344b6db6")]
        public virtual System.String Position_Company_Name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Должность.Отдел.Наименование' (псевдоним: Position_Department_Name)
        /// </summary>
        [Bars.B4.Utils.Display("Должность.Отдел.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("a376395b-455f-4870-8375-0a691fd1f3b5")]
        public virtual System.String Position_Department_Name
        {
            get;
            set;
        }

        /// <summary>
        /// Поле 'Должность.Наименование' (псевдоним: Position_Name)
        /// </summary>
        [Bars.B4.Utils.Display("Должность.Наименование")]
        [Bars.Rms.Core.Attributes.Uid("276419cd-9fa2-407f-8432-4bac9be1ca16")]
        public virtual System.String Position_Name
        {
            get;
            set;
        }

        /// <summary>
        /// Является ли запись листом дерева (не имеет вложенных)
        /// </summary>
        [Bars.B4.Utils.Display("Является ли запись листом дерева (не имеет вложенных)")]
        [Bars.Rms.Core.Attributes.Uid("IsLeaf")]
        [JsonProperty("leaf")]
        public virtual bool IsLeaf
        {
            get;
            set;
        }
    }
}