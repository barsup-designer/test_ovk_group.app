namespace Bars.avtor
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Сотрудник'
    /// </summary>
    public interface IEmployeeListQuery : IQueryOperation<Bars.avtor.Employee, Bars.avtor.EmployeeListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Сотрудник'
    /// </summary>
    public interface IEmployeeListQueryFilter : IQueryOperationFilter<Bars.avtor.Employee, Bars.avtor.EmployeeListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Сотрудник'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Сотрудник")]
    [ApiAccessible("Реестр Сотрудник")]
    public class EmployeeListQuery : RmsEntityQueryOperation<Bars.avtor.Employee, Bars.avtor.EmployeeListModel>, IEmployeeListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "EmployeeListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public EmployeeListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.avtor.Employee> Filter(IQueryable<Bars.avtor.Employee> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.avtor.EmployeeListModel> Map(IQueryable<Bars.avtor.Employee> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.avtor.Employee>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.avtor.EmployeeListModel{Id = x.Id, _TypeUid = "5f551863-5f51-43cc-b96c-ec1be7b2fe29", LastName = ((System.String)(x.LastName)), FirestName = ((System.String)(x.FirestName)), Patronymic = ((System.String)(x.Patronymic)), Position_Company_Name = ((System.String)(x.Position.Company.Name)), Position_Department_Name = ((System.String)(x.Position.Department.Name)), Position_Name = ((System.String)(x.Position.Name)), });
        }
    }
}