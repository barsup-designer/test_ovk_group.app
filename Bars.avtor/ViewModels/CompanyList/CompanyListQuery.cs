namespace Bars.avtor
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Компания'
    /// </summary>
    public interface ICompanyListQuery : IQueryOperation<Bars.avtor.Company, Bars.avtor.CompanyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Компания'
    /// </summary>
    public interface ICompanyListQueryFilter : IQueryOperationFilter<Bars.avtor.Company, Bars.avtor.CompanyListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Компания'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Компания")]
    public class CompanyListQuery : RmsEntityQueryOperation<Bars.avtor.Company, Bars.avtor.CompanyListModel>, ICompanyListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "CompanyListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public CompanyListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.avtor.Company> Filter(IQueryable<Bars.avtor.Company> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.avtor.CompanyListModel> Map(IQueryable<Bars.avtor.Company> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.avtor.Company>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.avtor.CompanyListModel{Id = x.Id, _TypeUid = "b915b492-7113-44f3-a73f-d89d0b46ec08", Name = ((System.String)(x.Name)), });
        }
    }
}