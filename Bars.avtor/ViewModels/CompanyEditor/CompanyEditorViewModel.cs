namespace Bars.avtor
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Компания'
    /// </summary>
    public interface ICompanyEditorViewModel : IEntityEditorViewModel<Bars.avtor.Company, CompanyEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Компания'
    /// </summary>
    public interface ICompanyEditorHandler : IEntityEditorViewModelHandler<Bars.avtor.Company, CompanyEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Компания'
    /// </summary>
    public abstract class AbstractCompanyEditorHandler : EntityEditorViewModelHandler<Bars.avtor.Company, CompanyEditorModel>, ICompanyEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Компания'
    /// </summary>
    public class CompanyEditorViewModel : BaseEditorViewModel<Bars.avtor.Company, CompanyEditorModel>, ICompanyEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override CompanyEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new CompanyEditorModel();
            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Компания' в модель представления
        /// </summary>
        protected override CompanyEditorModel MapEntityInternal(Bars.avtor.Company entity)
        {
            // создаем экзепляр модели
            var model = new CompanyEditorModel();
            model.Id = entity.Id;
            model.Name = ((System.String)(entity.Name));
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Компания' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.avtor.Company entity, CompanyEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Name = model.Name;
        }
    }
}