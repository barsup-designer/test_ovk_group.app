namespace Bars.avtor
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Сотрудник' для отдачи на клиент
    /// </summary>
    public class EmployeeEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public EmployeeEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Фамилия'
        /// </summary>
        [Bars.B4.Utils.Display("Фамилия")]
        [Bars.Rms.Core.Attributes.Uid("d4eeee7f-3e8d-47c5-8f71-6856b5f099ae")]
        public virtual System.String LastName
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Имя'
        /// </summary>
        [Bars.B4.Utils.Display("Имя")]
        [Bars.Rms.Core.Attributes.Uid("619c6335-fad8-40ef-a708-47abaa81cd0a")]
        public virtual System.String FirestName
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Отчество'
        /// </summary>
        [Bars.B4.Utils.Display("Отчество")]
        [Bars.Rms.Core.Attributes.Uid("38554103-d183-467e-92e2-d88c2ad698f9")]
        public virtual System.String Patronymic
        {
            get;
            set;
        }

        /// <summary>
        /// Должность
        /// </summary>
        [Bars.B4.Utils.Display("Должность")]
        [Bars.Rms.Core.Attributes.Uid("5f1743a9-0958-4ce4-a29a-1376b7b78c16")]
        public virtual Bars.avtor.PositionListModel Position
        {
            get;
            set;
        }
    }
}