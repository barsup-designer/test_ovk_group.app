namespace Bars.avtor
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Сотрудник'
    /// </summary>
    public interface IEmployeeEditorViewModel : IEntityEditorViewModel<Bars.avtor.Employee, EmployeeEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Сотрудник'
    /// </summary>
    public interface IEmployeeEditorHandler : IEntityEditorViewModelHandler<Bars.avtor.Employee, EmployeeEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Сотрудник'
    /// </summary>
    public abstract class AbstractEmployeeEditorHandler : EntityEditorViewModelHandler<Bars.avtor.Employee, EmployeeEditorModel>, IEmployeeEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Сотрудник'
    /// </summary>
    public class EmployeeEditorViewModel : BaseEditorViewModel<Bars.avtor.Employee, EmployeeEditorModel>, IEmployeeEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override EmployeeEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new EmployeeEditorModel();
            var varPositionId = @params.Params.GetAs<long>("Position_Id", 0);
            if (varPositionId > 0)
            {
                model.Position = Container.Resolve<Bars.avtor.IPositionListQuery>().GetById(varPositionId);
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Сотрудник' в модель представления
        /// </summary>
        protected override EmployeeEditorModel MapEntityInternal(Bars.avtor.Employee entity)
        {
            // создаем экзепляр модели
            var model = new EmployeeEditorModel();
            model.Id = entity.Id;
            model.LastName = ((System.String)(entity.LastName));
            model.FirestName = ((System.String)(entity.FirestName));
            model.Patronymic = ((System.String)(entity.Patronymic));
            if (entity.Position.IsNotNull())
            {
                var queryPositionList = Container.Resolve<Bars.avtor.IPositionListQuery>();
                model.Position = queryPositionList.GetById(entity.Position.Id);
            }

            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Сотрудник' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.avtor.Employee entity, EmployeeEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.LastName = model.LastName;
            entity.FirestName = model.FirestName;
            entity.Patronymic = model.Patronymic;
            entity.Position = TryLoadEntityById<Bars.avtor.Position>(model.Position?.Id);
        }
    }
}