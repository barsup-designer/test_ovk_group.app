namespace Bars.avtor
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Модель редактора 'Форма редактирования Должность' для отдачи на клиент
    /// </summary>
    public class PositionEditorModel : IEditorModel
    {
        /// <summary>
        /// Создание нового экземпляра модели
        /// </summary>
        public PositionEditorModel()
        {
        }

        /// <summary>
        /// Идентификатор записи
        /// </summary>
        [Bars.B4.Utils.Display("Идентификатор записи")]
        [Bars.Rms.Core.Attributes.Uid("Id")]
        public virtual System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Наименование'
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("2c74d424-f220-4aa5-9a15-035cbf89b7df")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Отдел
        /// </summary>
        [Bars.B4.Utils.Display("Отдел")]
        [Bars.Rms.Core.Attributes.Uid("ba9793b0-1f73-4af1-bcfc-de115817144b")]
        public virtual Bars.avtor.PositionEditorDepartmentControlModel Department
        {
            get;
            set;
        }

        /// <summary>
        /// Компания
        /// </summary>
        [Bars.B4.Utils.Display("Компания")]
        [Bars.Rms.Core.Attributes.Uid("bbcaa488-6f8a-490a-b7bf-948d47517598")]
        public virtual Bars.avtor.PositionEditorCompanyControlModel Company
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Отдел'
    /// </summary>
    public class PositionEditorDepartmentControlModel
    {
        /// <summary>
        /// Свойство 'Должность.Отдел.Идентификатор'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Должность.Отдел.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Должность.Отдел.Наименование'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Должность.Отдел.Наименование")]
        public System.String Name
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Модель для отображения свойства 'Компания'
    /// </summary>
    public class PositionEditorCompanyControlModel
    {
        /// <summary>
        /// Свойство 'Должность.Компания.Идентификатор'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Должность.Компания.Идентификатор")]
        public System.Int64? Id
        {
            get;
            set;
        }

        /// <summary>
        /// Свойство 'Должность.Компания.Наименование'
        /// </summary>
        [Bars.B4.Utils.DisplayAttribute("Должность.Компания.Наименование")]
        public System.String Name
        {
            get;
            set;
        }
    }
}