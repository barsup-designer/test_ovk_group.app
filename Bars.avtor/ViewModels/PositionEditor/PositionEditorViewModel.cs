namespace Bars.avtor
{
    using Newtonsoft.Json;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Modules.Filter;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Editors;
    using Bars.Rms.GeneratedApp.Exceptions;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Newtonsoft.Json.Linq;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using Castle.Windsor;

    /// <summary>
    /// Контракт модели представления 'Форма редактирования Должность'
    /// </summary>
    public interface IPositionEditorViewModel : IEntityEditorViewModel<Bars.avtor.Position, PositionEditorModel>
    {
    }

    /// <summary>
    /// Интерфейс обработчика редактора 'Форма редактирования Должность'
    /// </summary>
    public interface IPositionEditorHandler : IEntityEditorViewModelHandler<Bars.avtor.Position, PositionEditorModel>
    {
    }

    /// <summary>
    /// Базовый класс обработчика редактора 'Форма редактирования Должность'
    /// </summary>
    public abstract class AbstractPositionEditorHandler : EntityEditorViewModelHandler<Bars.avtor.Position, PositionEditorModel>, IPositionEditorHandler
    {
    }

    /// <summary>
    /// Реализация модели представления 'Форма редактирования Должность'
    /// </summary>
    public class PositionEditorViewModel : BaseEditorViewModel<Bars.avtor.Position, PositionEditorModel>, IPositionEditorViewModel
    {
        /// <summary>
        /// Создание новой модели и её заполнение
        /// </summary>
        protected override PositionEditorModel CreateModelInternal(BaseParams @params)
        {
            var model = new PositionEditorModel();
            var varDepartmentId = @params.Params.GetAs<long>("Department_Id", 0);
            if (varDepartmentId > 0)
            {
                var queryDepartment = Container.ResolveDomain<Bars.avtor.Department>().GetAll().Where(x => x.Id == varDepartmentId);
                model.Department = queryDepartment.Select(x => new PositionEditorDepartmentControlModel{Id = x.Id, Name = x.Name, }).FirstOrDefault();
            }

            var varCompanyId = @params.Params.GetAs<long>("Company_Id", 0);
            if (varCompanyId > 0)
            {
                var queryCompany = Container.ResolveDomain<Bars.avtor.Company>().GetAll().Where(x => x.Id == varCompanyId);
                model.Company = queryCompany.Select(x => new PositionEditorCompanyControlModel{Id = x.Id, Name = x.Name, }).FirstOrDefault();
            }

            return model;
        }

        /// <summary>
        /// Преобразование сущности 'Должность' в модель представления
        /// </summary>
        protected override PositionEditorModel MapEntityInternal(Bars.avtor.Position entity)
        {
            // создаем экзепляр модели
            var model = new PositionEditorModel();
            model.Id = entity.Id;
            model.Name = ((System.String)(entity.Name));
            model.Department = entity.Department == null ? null : new PositionEditorDepartmentControlModel{Id = (System.Int64? )(entity.Return(p => p.Department).Return(p => p.Id)), Name = entity.Return(p => p.Department).Return(p => p.Name), };
            model.Company = entity.Company == null ? null : new PositionEditorCompanyControlModel{Id = (System.Int64? )(entity.Return(p => p.Company).Return(p => p.Id)), Name = entity.Return(p => p.Company).Return(p => p.Name), };
            return model;
        }

        /// <summary>
        /// Восстановление сущности 'Должность' из модели представления		
        /// </summary>
        protected override void UnmapEntityInternal(Bars.avtor.Position entity, PositionEditorModel model, IDictionary<string, FileData> requestFiles, IList<Bars.B4.Modules.FileStorage.FileInfo> filesToDelete, bool isNested = false)
        {
            entity.Name = model.Name;
            entity.Department = TryLoadEntityById<Bars.avtor.Department>(model.Department?.Id);
            entity.Company = TryLoadEntityById<Bars.avtor.Company>(model.Company?.Id);
        }
    }
}