namespace Bars.avtor
{
    using Castle.Windsor;
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Utils;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.Attributes;
    using Bars.Rms.GeneratedApp.Queries;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Linq.Expressions;
    using System.Linq;
    using System.Web.Mvc;
    using System;
    using NHibernate.Linq;
    using Bars.Rms.GeneratedApp;

    /// <summary>
    /// Интерфейс запроса данных для представления 'Реестр Отдел'
    /// </summary>
    public interface IDepartmentListQuery : IQueryOperation<Bars.avtor.Department, Bars.avtor.DepartmentListModel, BaseParams>
    {
    }

    /// <summary>
    /// Интерфейс фильтра запроса данных для представления 'Реестр Отдел'
    /// </summary>
    public interface IDepartmentListQueryFilter : IQueryOperationFilter<Bars.avtor.Department, Bars.avtor.DepartmentListModel, BaseParams>
    {
    }

    /// <summary>
    /// Запрос данных для представления 'Реестр Отдел'
    /// </summary>
    [Bars.B4.Utils.DisplayAttribute("Реестр Отдел")]
    public class DepartmentListQuery : RmsEntityQueryOperation<Bars.avtor.Department, Bars.avtor.DepartmentListModel>, IDepartmentListQuery
    {
        /// <summary>
        /// Сериализованные в json системные фильтры
        /// </summary>
        protected override string FiltersJson
        {
            get
            {
                return @"[]
";
            }
        }

        /// <summary>
        /// Создание нового экземпляра <see cref = "DepartmentListQuery"/>
        /// </summary>        
        /// <param name = "container">Контейнер зависимостей</param>
        /// <param name = "permissions">Список разрешений для выполнения запроса</param>
        public DepartmentListQuery(IWindsorContainer container, string[] permissions = null): base (container, permissions)
        {
        }

        /// <summary>
        /// Фильтрация исходного запроса
        /// </summary>
        /// <param name = "entityQuery">Запрос сущностей</param>
        /// <param name = "params">Параметры операции</param>
        /// <returns></returns>
        protected override IQueryable<Bars.avtor.Department> Filter(IQueryable<Bars.avtor.Department> entityQuery, BaseParams @params)
        {
            var query = base.Filter(entityQuery, @params);
            if (@params.IsNotNull())
            {
            }

            return query;
        }

        /// <summary>
        /// Формирование запроса на получение моделей
        /// </summary>
        public override IQueryable<Bars.avtor.DepartmentListModel> Map(IQueryable<Bars.avtor.Department> entityQuery, BaseParams @params)
        {
            var query = Container.ResolveDomain<Bars.avtor.Department>().GetAll();
            // формирование селектора			
            return entityQuery.Select(x => new Bars.avtor.DepartmentListModel{Id = x.Id, _TypeUid = "e7fafdcd-80ed-4ff1-8d3b-1e2997903fb7", Name = ((System.String)(x.Name)), });
        }
    }
}