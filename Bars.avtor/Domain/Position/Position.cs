namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Должность
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Должность")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("6fe4c6e0-2fdf-477a-bf72-8ba97ae13f0e")]
    [Bars.Rms.Core.Attributes.Uid("6fe4c6e0-2fdf-477a-bf72-8ba97ae13f0e")]
    public class Position : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Position(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("fa415c7b-cd95-43f6-8891-6ddab9e530f8")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Отдел
        /// </summary>
        [Bars.B4.Utils.Display("Отдел")]
        [Bars.Rms.Core.Attributes.Uid("bba51e9c-4ebe-47bc-880f-1b9341574647")]
        public virtual Bars.avtor.Department Department
        {
            get;
            set;
        }

        /// <summary>
        /// Компания
        /// </summary>
        [Bars.B4.Utils.Display("Компания")]
        [Bars.Rms.Core.Attributes.Uid("d4f8e7e4-d53a-4fbd-b4fa-ce8b45a0493f")]
        public virtual Bars.avtor.Company Company
        {
            get;
            set;
        }
    }
}