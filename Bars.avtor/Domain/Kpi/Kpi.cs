namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// KPI
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"KPI")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("01fd952e-1817-48e0-84fc-6713a01e9b1d")]
    [Bars.Rms.Core.Attributes.Uid("01fd952e-1817-48e0-84fc-6713a01e9b1d")]
    public class Kpi : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Kpi(): base ()
        {
        }
    }
}