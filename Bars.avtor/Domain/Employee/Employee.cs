namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Сотрудник
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Сотрудник")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("5f551863-5f51-43cc-b96c-ec1be7b2fe29")]
    [Bars.Rms.Core.Attributes.Uid("5f551863-5f51-43cc-b96c-ec1be7b2fe29")]
    public class Employee : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Employee(): base ()
        {
        }

        /// <summary>
        /// Фамилия
        /// </summary>
        [Bars.B4.Utils.Display("Фамилия")]
        [Bars.Rms.Core.Attributes.Uid("dc221e1a-d04e-4a13-8e8d-1351f3c410f8")]
        public virtual System.String LastName
        {
            get;
            set;
        }

        /// <summary>
        /// Имя
        /// </summary>
        [Bars.B4.Utils.Display("Имя")]
        [Bars.Rms.Core.Attributes.Uid("f6c88cf8-295c-40a2-a84c-d843c90709fd")]
        public virtual System.String FirestName
        {
            get;
            set;
        }

        /// <summary>
        /// Отчество
        /// </summary>
        [Bars.B4.Utils.Display("Отчество")]
        [Bars.Rms.Core.Attributes.Uid("db263b66-54f0-4f50-a18c-06ff2c9537e1")]
        public virtual System.String Patronymic
        {
            get;
            set;
        }

        /// <summary>
        /// Должность
        /// </summary>
        [Bars.B4.Utils.Display("Должность")]
        [Bars.Rms.Core.Attributes.Uid("61d6fcac-6154-4abc-8368-efd58f5b0028")]
        public virtual Bars.avtor.Position Position
        {
            get;
            set;
        }
    }
}