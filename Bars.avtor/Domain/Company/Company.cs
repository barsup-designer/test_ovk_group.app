namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Компания
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Компания")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("b915b492-7113-44f3-a73f-d89d0b46ec08")]
    [Bars.Rms.Core.Attributes.Uid("b915b492-7113-44f3-a73f-d89d0b46ec08")]
    public class Company : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Company(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("65b673f9-f5b7-43b1-9d39-b16ec0fb507c")]
        public virtual System.String Name
        {
            get;
            set;
        }
    }
}