namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using System.Linq;
    using System;

    /// <summary>
    /// Отдел
    /// 
    /// </summary>
    [Bars.B4.Utils.Display(@"Отдел")]
    [Bars.B4.Utils.Description(@"")]
    [System.Runtime.InteropServices.GuidAttribute("e7fafdcd-80ed-4ff1-8d3b-1e2997903fb7")]
    [Bars.Rms.Core.Attributes.Uid("e7fafdcd-80ed-4ff1-8d3b-1e2997903fb7")]
    public class Department : Bars.B4.DataAccess.BaseEntity
    {
        /// <summary>
        /// Конструктор сущности
        /// </summary>
        public Department(): base ()
        {
        }

        /// <summary>
        /// Наименование
        /// </summary>
        [Bars.B4.Utils.Display("Наименование")]
        [Bars.Rms.Core.Attributes.Uid("42a3dcb7-a110-46e4-ae0d-d91d43c81119")]
        public virtual System.String Name
        {
            get;
            set;
        }

        /// <summary>
        /// Компания
        /// </summary>
        [Bars.B4.Utils.Display("Компания")]
        [Bars.Rms.Core.Attributes.Uid("4f65c982-7e38-4eca-bd29-d84e2a57c7a1")]
        public virtual Bars.avtor.Company Company
        {
            get;
            set;
        }
    }
}