namespace Bars.avtor
{
    using Bars.B4.Utils;
    using Bars.B4;

    /// <summary>
    /// Манифест ресурсов модуля
    /// </summary>
    public partial class ResourceManifest
    {
        /// <summary>
        /// Базовая инициализация. 
        ///             Обычно вызывается из T4-шаблонов.
        /// </summary>
        /// <param name = "container"/>
        protected override void BaseInit(IResourceManifestContainer container)
        {
            AddResource(container, "content\\css\\app-icons.css");
            AddResource(container, "libs\\B4\\controller\\CompanyEditor.js");
            AddResource(container, "libs\\B4\\controller\\CompanyList.js");
            AddResource(container, "libs\\B4\\controller\\DepartmentEditor.js");
            AddResource(container, "libs\\B4\\controller\\DepartmentList.js");
            AddResource(container, "libs\\B4\\controller\\EmployeeEditor.js");
            AddResource(container, "libs\\B4\\controller\\EmployeeList.js");
            AddResource(container, "libs\\B4\\controller\\PositionEditor.js");
            AddResource(container, "libs\\B4\\controller\\PositionList.js");
            AddResource(container, "libs\\B4\\CustomVTypes.js");
            AddResource(container, "libs\\B4\\view\\CompanyEditor.js");
            AddResource(container, "libs\\B4\\view\\CompanyList.js");
            AddResource(container, "libs\\B4\\view\\DepartmentEditor.js");
            AddResource(container, "libs\\B4\\view\\DepartmentList.js");
            AddResource(container, "libs\\B4\\view\\EmployeeEditor.js");
            AddResource(container, "libs\\B4\\view\\EmployeeList.js");
            AddResource(container, "libs\\B4\\view\\PositionEditor.js");
            AddResource(container, "libs\\B4\\view\\PositionList.js");
        }

        private void AddResource(IResourceManifestContainer container, string path)
        {
            var webPath = path.Replace("\\", "/");
            var resourceName = webPath.Replace("/", ".");
            container.Add(webPath, "Bars.avtor.dll/Bars.avtor.{0}".FormatUsing(resourceName));
        }
    }
}