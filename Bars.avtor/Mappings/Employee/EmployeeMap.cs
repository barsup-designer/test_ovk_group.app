namespace Bars.avtor.Mappings
{
    using Bars.B4.DataAccess.ByCode;
    using Bars.B4.DataAccess.UserTypes;
    using Bars.B4.DataAccess;
    using Bars.B4.Modules.PostgreSql.DataAccess.UserTypes;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp;
    using NHibernate.Mapping.ByCode.Conformist;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Type;

    /// <summary>
    /// Мапинг сущности Сотрудник
    /// </summary>
    public class EmployeeMap : ClassMapping<Bars.avtor.Employee>
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public EmployeeMap()
        {
            Polymorphism(PolymorphismType.Explicit);
            Table("EMPLOYEE");
            Id(x => x.Id, map =>
            {
                map.Column("id");
                map.Type(new Int64Type());
                map.Generator(new Bars.B4.Modules.NH.IdGenerators.BulkSeq.BulkSequenceIdGeneratorDef());
            }

            );
#region Class: Employee
            Property(x => x.LastName, p =>
            {
                p.Column(col =>
                {
                    col.Name("LASTNAME");
                }

                );
            }

            );
            Property(x => x.FirestName, p =>
            {
                p.Column(col =>
                {
                    col.Name("FIRESTNAME");
                }

                );
            }

            );
            Property(x => x.Patronymic, p =>
            {
                p.Column(col =>
                {
                    col.Name("PATRONYMIC");
                }

                );
            }

            );
            ManyToOne(x => x.Position, m =>
            {
                m.Column("POSITION");
                m.Cascade(Cascade.Persist);
            }

            );
#endregion
#region Class: BaseEntity
            Property(x => x.ObjectCreateDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_CREATE_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectEditDate, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_EDIT_DATE");
                }

                );
                p.NotNullable(true);
            }

            );
            Property(x => x.ObjectVersion, p =>
            {
                p.Column(col =>
                {
                    col.Name("OBJECT_VERSION");
                }

                );
                p.NotNullable(true);
            }

            );
#endregion
        }
    }
}