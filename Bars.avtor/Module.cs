namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Registrar;
    using Bars.B4.ResourceBundling;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    [Bars.B4.Utils.Display("Зарплатный Профиль")]
    [Bars.B4.Utils.Description("")]
    [Bars.B4.Utils.CustomValue("Version", "2.2018.0702.31")]
    [Bars.Rms.Core.Attributes.Uid("2d2ab6c9-fa8b-4d57-ab3b-a2ca851839c0")]
    public partial class Module : AssemblyDefinedModule
    {
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            Container.RegisterResourceManifest<Bars.avtor.ResourceManifest>();
            Container.RegisterTransient<Bars.B4.License.ILicenseInfo, LicenceInformation>();
            RegisterControllers();
            RegisterDomainServices();
            RegisterNavigationProviders();
            RegisterPermissionMaps();
            RegisterVariables();
            RegisterExternalResources();
        }

        protected override void SetPredecessors()
        {
            base.SetPredecessors();
            SetPredecessor<Bars.Rms.GeneratedApp.Module>();
        }
    }
}