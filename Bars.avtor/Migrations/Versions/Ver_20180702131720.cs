namespace Bars.avtor.Migrations
{
    using Bars.B4.Modules.Ecm7.Framework;
    using Bars.B4.Modules.NH.Migrations.DatabaseExtensions;
    using Bars.B4.Modules.PostgreSql.DataAccess;
    using Bars.B4.Modules.PostgreSql.Migrations;
    using Bars.B4.Modules.PostgreSql;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp.Migrations;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using System;

    /// <summary>
    /// Миграция 2018.07.02.13-17-20
    /// </summary>
    [Bars.B4.Modules.Ecm7.Framework.Migration("2018.07.02.13-17-20")]
    public class Ver20180702131720 : BaseMigration
    {
        /// <summary>
        /// Накатить миграцию
        /// </summary>
        public override void Up()
        {
            EnsureDbTypes();
            Database.AddTable("EMPLOYEE");
            Database.ExecuteNonQuery("COMMENT ON TABLE EMPLOYEE is E\'\"Сотрудник\"\'");
            Database.AddTable("POSITION");
            Database.ExecuteNonQuery("COMMENT ON TABLE POSITION is E\'\"Должность\"\'");
            Database.AddTable("COMPANY");
            Database.ExecuteNonQuery("COMMENT ON TABLE COMPANY is E\'\"Компания\"\'");
            Database.AddTable("DEPARTMENT");
            Database.ExecuteNonQuery("COMMENT ON TABLE DEPARTMENT is E\'\"Отдел\"\'");
            Database.AddColumn("EMPLOYEE", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN EMPLOYEE.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("EMPLOYEE", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN EMPLOYEE.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("EMPLOYEE", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN EMPLOYEE.object_edit_date is E\'\"Дата последнего редактирования\"\'");
            Database.AddColumn("EMPLOYEE", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN EMPLOYEE.object_version is E\'\"Версия объекта\"\'");
            Database.AddColumn("EMPLOYEE", new Column("lastname", DbType.String.AsColumnType().WithLength(100), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN EMPLOYEE.lastname is E\'\"Фамилия\"\'");
            Database.AddColumn("EMPLOYEE", new Column("firestname", DbType.String.AsColumnType().WithLength(100), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN EMPLOYEE.firestname is E\'\"Имя\"\'");
            Database.AddColumn("EMPLOYEE", new Column("patronymic", DbType.String.AsColumnType().WithLength(100), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN EMPLOYEE.patronymic is E\'\"Отчество\"\'");
            Database.AddColumn("EMPLOYEE", new Column("position", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN EMPLOYEE.position is E\'\"Должность\"\'");
            Database.AddColumn("POSITION", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN POSITION.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("POSITION", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN POSITION.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("POSITION", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN POSITION.object_edit_date is E\'\"Дата последнего редактирования\"\'");
            Database.AddColumn("POSITION", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN POSITION.object_version is E\'\"Версия объекта\"\'");
            Database.AddColumn("POSITION", new Column("name", DbType.String.AsColumnType().WithLength(200), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN POSITION.name is E\'\"Наименование\"\'");
            Database.AddColumn("POSITION", new Column("department", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN POSITION.department is E\'\"Отдел\"\'");
            Database.AddColumn("POSITION", new Column("company", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN POSITION.company is E\'\"Компания\"\'");
            Database.AddColumn("COMPANY", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMPANY.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("COMPANY", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMPANY.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("COMPANY", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMPANY.object_edit_date is E\'\"Дата последнего редактирования\"\'");
            Database.AddColumn("COMPANY", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMPANY.object_version is E\'\"Версия объекта\"\'");
            Database.AddColumn("COMPANY", new Column("name", DbType.String.AsColumnType().WithLength(200), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN COMPANY.name is E\'\"Наименование\"\'");
            Database.AddColumn("DEPARTMENT", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN DEPARTMENT.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("DEPARTMENT", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN DEPARTMENT.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("DEPARTMENT", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN DEPARTMENT.object_edit_date is E\'\"Дата последнего редактирования\"\'");
            Database.AddColumn("DEPARTMENT", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN DEPARTMENT.object_version is E\'\"Версия объекта\"\'");
            Database.AddColumn("DEPARTMENT", new Column("name", DbType.String.AsColumnType().WithLength(200), ColumnProperty.None, "\'\'"));
            Database.ExecuteNonQuery("COMMENT ON COLUMN DEPARTMENT.name is E\'\"Наименование\"\'");
            Database.AddColumn("DEPARTMENT", new Column("company", DbType.Int64.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN DEPARTMENT.company is E\'\"Компания\"\'");
            Database.ChangeColumnNotNullable("EMPLOYEE", "object_create_date", true);
            Database.ChangeColumnNotNullable("EMPLOYEE", "object_edit_date", true);
            Database.ChangeColumnNotNullable("EMPLOYEE", "object_version", true);
            Database.ChangeColumnNotNullable("POSITION", "object_create_date", true);
            Database.ChangeColumnNotNullable("POSITION", "object_edit_date", true);
            Database.ChangeColumnNotNullable("POSITION", "object_version", true);
            Database.ChangeColumnNotNullable("COMPANY", "object_create_date", true);
            Database.ChangeColumnNotNullable("COMPANY", "object_edit_date", true);
            Database.ChangeColumnNotNullable("COMPANY", "object_version", true);
            Database.ChangeColumnNotNullable("COMPANY", "name", true);
            Database.ChangeColumnNotNullable("DEPARTMENT", "object_create_date", true);
            Database.ChangeColumnNotNullable("DEPARTMENT", "object_edit_date", true);
            Database.ChangeColumnNotNullable("DEPARTMENT", "object_version", true);
        }

        /// <summary>
        /// Откатить миграцию
        /// </summary>
        public override void Down()
        {
            EnsureDbTypes();
            Database.ChangeColumnNotNullable("DEPARTMENT", "object_version", false);
            Database.ChangeColumnNotNullable("DEPARTMENT", "object_edit_date", false);
            Database.ChangeColumnNotNullable("DEPARTMENT", "object_create_date", false);
            Database.ChangeColumnNotNullable("COMPANY", "name", false);
            Database.ChangeColumnNotNullable("COMPANY", "object_version", false);
            Database.ChangeColumnNotNullable("COMPANY", "object_edit_date", false);
            Database.ChangeColumnNotNullable("COMPANY", "object_create_date", false);
            Database.ChangeColumnNotNullable("POSITION", "object_version", false);
            Database.ChangeColumnNotNullable("POSITION", "object_edit_date", false);
            Database.ChangeColumnNotNullable("POSITION", "object_create_date", false);
            Database.ChangeColumnNotNullable("EMPLOYEE", "object_version", false);
            Database.ChangeColumnNotNullable("EMPLOYEE", "object_edit_date", false);
            Database.ChangeColumnNotNullable("EMPLOYEE", "object_create_date", false);
            Database.RemoveColumn("DEPARTMENT", "company");
            Database.RemoveColumn("DEPARTMENT", "name");
            Database.RemoveColumn("DEPARTMENT", "object_version");
            Database.RemoveColumn("DEPARTMENT", "object_edit_date");
            Database.RemoveColumn("DEPARTMENT", "object_create_date");
            Database.RemoveColumn("COMPANY", "name");
            Database.RemoveColumn("COMPANY", "object_version");
            Database.RemoveColumn("COMPANY", "object_edit_date");
            Database.RemoveColumn("COMPANY", "object_create_date");
            Database.RemoveColumn("POSITION", "company");
            Database.RemoveColumn("POSITION", "department");
            Database.RemoveColumn("POSITION", "name");
            Database.RemoveColumn("POSITION", "object_version");
            Database.RemoveColumn("POSITION", "object_edit_date");
            Database.RemoveColumn("POSITION", "object_create_date");
            Database.RemoveColumn("EMPLOYEE", "position");
            Database.RemoveColumn("EMPLOYEE", "patronymic");
            Database.RemoveColumn("EMPLOYEE", "firestname");
            Database.RemoveColumn("EMPLOYEE", "lastname");
            Database.RemoveColumn("EMPLOYEE", "object_version");
            Database.RemoveColumn("EMPLOYEE", "object_edit_date");
            Database.RemoveColumn("EMPLOYEE", "object_create_date");
            Database.RemoveTable("DEPARTMENT");
            Database.RemoveTable("COMPANY");
            Database.RemoveTable("POSITION");
            Database.RemoveTable("EMPLOYEE");
        }
    }
}