namespace Bars.avtor.Migrations
{
    using Bars.B4.Modules.Ecm7.Framework;
    using Bars.B4.Modules.NH.Migrations.DatabaseExtensions;
    using Bars.B4.Modules.PostgreSql.DataAccess;
    using Bars.B4.Modules.PostgreSql.Migrations;
    using Bars.B4.Modules.PostgreSql;
    using Bars.B4.Utils;
    using Bars.Rms.GeneratedApp.Migrations;
    using System.Collections.Generic;
    using System.Data;
    using System.Text;
    using System;

    /// <summary>
    /// Миграция 2018.07.02.13-24-01
    /// </summary>
    [Bars.B4.Modules.Ecm7.Framework.Migration("2018.07.02.13-24-01")]
    public class Ver20180702132401 : BaseMigration
    {
        /// <summary>
        /// Накатить миграцию
        /// </summary>
        public override void Up()
        {
            EnsureDbTypes();
            Database.AddTable("KPI");
            Database.ExecuteNonQuery("COMMENT ON TABLE KPI is E\'\"KPI\"\'");
            Database.AddColumn("KPI", new Column("id", DbType.Int64.AsColumnType(), ColumnProperty.Identity | ColumnProperty.PrimaryKey));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KPI.id is E\'\"Идентификатор\"\'");
            Database.AddColumn("KPI", new Column("object_create_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KPI.object_create_date is E\'\"Дата создания\"\'");
            Database.AddColumn("KPI", new Column("object_edit_date", DbType.DateTime.AsColumnType(), ColumnProperty.None));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KPI.object_edit_date is E\'\"Дата последнего редактирования\"\'");
            Database.AddColumn("KPI", new Column("object_version", DbType.Int32.AsColumnType(), ColumnProperty.None, 0));
            Database.ExecuteNonQuery("COMMENT ON COLUMN KPI.object_version is E\'\"Версия объекта\"\'");
            Database.ChangeColumnNotNullable("KPI", "object_create_date", true);
            Database.ChangeColumnNotNullable("KPI", "object_edit_date", true);
            Database.ChangeColumnNotNullable("KPI", "object_version", true);
        }

        /// <summary>
        /// Откатить миграцию
        /// </summary>
        public override void Down()
        {
            EnsureDbTypes();
            Database.ChangeColumnNotNullable("KPI", "object_version", false);
            Database.ChangeColumnNotNullable("KPI", "object_edit_date", false);
            Database.ChangeColumnNotNullable("KPI", "object_create_date", false);
            Database.RemoveColumn("KPI", "object_version");
            Database.RemoveColumn("KPI", "object_edit_date");
            Database.RemoveColumn("KPI", "object_create_date");
            Database.RemoveTable("KPI");
        }
    }
}