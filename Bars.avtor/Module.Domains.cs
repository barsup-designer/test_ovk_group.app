namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using Bars.B4.IoC;
    using Bars.B4.Windsor;
    using Bars.B4;
    using Bars.Rms.Core.TypeSystem;
    using Bars.Rms.Core;
    using Bars.Rms.GeneratedApp.States;
    using Bars.Rms.GeneratedApp;
    using Castle.MicroKernel.Registration;
    using Castle.Windsor;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Web.Mvc;
    using System;

    public partial class Module
    {
        protected virtual void RegisterDomainServices()
        {
            Container.RegisterQueryOperation<Bars.avtor.PositionListQuery>();
            Container.RegisterViewModel<Bars.avtor.PositionListViewModel>();
            Component.For<Bars.B4.IDomainServiceInterceptor<Kpi>>().ImplementedBy<Bars.avtor.KpiDomainServiceInterceptor>().LifestyleTransient().RegisterIn(Container);
            Container.RegisterEditorViewModel<Bars.avtor.DepartmentEditorViewModel>();
            Component.For<Bars.B4.IDomainServiceInterceptor<Position>>().ImplementedBy<Bars.avtor.PositionDomainServiceInterceptor>().LifestyleTransient().RegisterIn(Container);
            Container.RegisterQueryOperation<Bars.avtor.EmployeeListQuery>();
            Component.For<Bars.B4.IDomainServiceInterceptor<Company>>().ImplementedBy<Bars.avtor.CompanyDomainServiceInterceptor>().LifestyleTransient().RegisterIn(Container);
            Container.RegisterEditorViewModel<Bars.avtor.CompanyEditorViewModel>();
            Container.RegisterViewModel<Bars.avtor.DepartmentListViewModel>();
            Container.RegisterQueryOperation<Bars.avtor.DepartmentListQuery>();
            Component.For<Bars.B4.IModuleDependencies>().ImplementedBy<Bars.avtor.ModuleDependencies>().LifestyleSingleton().RegisterIn(Container);
            Container.RegisterEditorViewModel<Bars.avtor.EmployeeEditorViewModel>();
            Container.RegisterEditorViewModel<Bars.avtor.PositionEditorViewModel>();
            Component.For<Bars.B4.IDomainServiceInterceptor<Department>>().ImplementedBy<Bars.avtor.DepartmentDomainServiceInterceptor>().LifestyleTransient().RegisterIn(Container);
            Component.For<Bars.B4.IDomainServiceInterceptor<Employee>>().ImplementedBy<Bars.avtor.EmployeeDomainServiceInterceptor>().LifestyleTransient().RegisterIn(Container);
            Component.For<Bars.B4.Modules.NHibernateChangeLog.IAuditLogMapProvider>().ImplementedBy<Bars.avtor.AuditLogMapProvider>().LifestyleSingleton().RegisterIn(Container);
            Container.RegisterViewModel<Bars.avtor.EmployeeListViewModel>();
            Component.For<Bars.B4.Modules.States.IStatefulEntitiesManifest>().ImplementedBy<Bars.avtor.States.StatesManifest>().LifestyleTransient().RegisterIn(Container);
            Container.RegisterViewModel<Bars.avtor.CompanyListViewModel>();
            Container.RegisterQueryOperation<Bars.avtor.CompanyListQuery>();
            Component.For<Bars.B4.DataAccess.INhibernateConfigModifier>().ImplementedBy<Bars.avtor.NHibernateConfigurator>().LifestyleTransient().RegisterIn(Container);
            NHibernateConfigurator.RegisterAll();
        }
    }
}