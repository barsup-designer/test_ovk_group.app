namespace Bars.avtor
{
    using Bars.B4;
    using Bars.B4.Modules.ExtJs;
    using Bars.B4.Modules.Security;
    using Bars.B4.Utils;
    using System.Collections.Generic;
    using System.Linq;

    public partial class ResourceManifest : ResourceManifestBase
    {
        protected override void AdditionalInit(IResourceManifestContainer container)
        {
#region Перечисления
#endregion
#region Модели
            container.RegisterExtJsModel<Bars.avtor.EmployeeEditorModel>().ActionMethods(read: "POST").Controller("EmployeeEditor");
            container.RegisterExtJsModel<DepartmentListModel>().ActionMethods(read: "POST").Controller("DepartmentList");
            container.RegisterExtJsModel<Bars.avtor.CompanyEditorModel>().ActionMethods(read: "POST").Controller("CompanyEditor");
            container.RegisterExtJsModel<Bars.avtor.DepartmentEditorModel>().ActionMethods(read: "POST").Controller("DepartmentEditor");
            container.RegisterExtJsModel<EmployeeListModel>().ActionMethods(read: "POST").Controller("EmployeeList");
            container.RegisterExtJsModel<CompanyListModel>().ActionMethods(read: "POST").Controller("CompanyList");
            container.RegisterExtJsModel<Bars.avtor.PositionEditorModel>().ActionMethods(read: "POST").Controller("PositionEditor");
            container.RegisterExtJsModel<PositionListModel>().ActionMethods(read: "POST").Controller("PositionList");
            container.RegisterExtJsModel<Bars.B4.Modules.FileStorage.FileInfo>().ActionMethods(read: "POST").Controller("Empty");
#endregion
        }
    }
}