namespace Bars.avtor
{
    using Bars.B4.DataAccess;
    using Bars.B4;
    using System.Linq.Expressions;
    using System.Linq;
    using Castle.Windsor;
    using System;

    public class ModuleDependencies : BaseModuleDependencies
    {
        public ModuleDependencies(IWindsorContainer container): base (container)
        {
            RegisterDepartmentDependencies();
            RegisterPositionDependencies();
            RegisterCompanyDependencies();
        }

#region Отдел и его наследники
        private void RegisterDepartmentDependencies()
        {
#region Отдел
            References.Add(new EntityReference{ReferenceName = "Должность -> Отдел", BaseEntity = typeof (Bars.avtor.Department), CheckAnyDependences = id => Any<Bars.avtor.Position>(x => x.Department.Id == id), GetCountDependences = id => Count<Bars.avtor.Position>(x => x.Department.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Отдел] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Должность и его наследники
        private void RegisterPositionDependencies()
        {
#region Должность
            References.Add(new EntityReference{ReferenceName = "Сотрудник -> Должность", BaseEntity = typeof (Bars.avtor.Position), CheckAnyDependences = id => Any<Bars.avtor.Employee>(x => x.Position.Id == id), GetCountDependences = id => Count<Bars.avtor.Employee>(x => x.Position.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Должность] так как на него есть ссылки"});
#endregion
        }

#endregion
#region Компания и его наследники
        private void RegisterCompanyDependencies()
        {
#region Компания
            References.Add(new EntityReference{ReferenceName = "Отдел -> Компания", BaseEntity = typeof (Bars.avtor.Company), CheckAnyDependences = id => Any<Bars.avtor.Department>(x => x.Company.Id == id), GetCountDependences = id => Count<Bars.avtor.Department>(x => x.Company.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Компания] так как на него есть ссылки"});
            References.Add(new EntityReference{ReferenceName = "Должность -> Компания", BaseEntity = typeof (Bars.avtor.Company), CheckAnyDependences = id => Any<Bars.avtor.Position>(x => x.Company.Id == id), GetCountDependences = id => Count<Bars.avtor.Position>(x => x.Company.Id == id), GetDescriptionDependences = id => "Не удалось удалить элемент [Компания] так как на него есть ссылки"});
#endregion
        }

#endregion
        private IQueryable<T> GetAll<T>() => Container.Resolve<IDataStore>().GetAll<T>();
        private bool Any<T>(Expression<Func<T, bool>> predicate) => GetAll<T>().Any(predicate);
        private int Count<T>(Expression<Func<T, bool>> predicate) => GetAll<T>().Count(predicate);
        private void SetNull<T>(Expression<Func<T, bool>> predicate, Action<T> action)where T : IEntity
        {
            var ds = Container.Resolve<IDataStore>();
            var records = ds.GetAll<T>().Where(predicate).ToArray();
            foreach (var record in records)
            {
                action.Invoke(record);
                ds.Update(record);
            }
        }

        private void RemoveRefs<T>(Expression<Func<T, bool>> predicate)where T : IEntity
        {
            var domainService = Container.ResolveDomain<T>();
            var entities = domainService.GetAll().Where(predicate).Select(x => new
            {
            Type = x.GetType(), Id = x.Id
            }

            ).ToArray().GroupBy(x => x.Type);
            foreach (var type in entities)
            {
                var x = type.Key;
                if (typeof (NHibernate.Proxy.INHibernateProxy).IsAssignableFrom(x))
                    x = type.Key.BaseType;
                var ds = Container.Resolve(typeof (IDomainService<>).MakeGenericType(x)) as IDomainService;
                foreach (var v in type)
                    ds.Delete(v.Id);
            }
        }
    }
}