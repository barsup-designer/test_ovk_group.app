Ext.define('B4.controller.EmployeeList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.EmployeeList',
        'B4.model.EmployeeListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-employeelist',
    viewDataName: 'EmployeeList',
    // набор описаний действий реестра
    actions: {
        'Addition-EmployeeEditor-InWindow': {
            'editor': 'EmployeeEditor',
            'editorAlias': 'rms-employeeeditor',
            'editorUid': 'f6f75a72-b8a3-41f4-a7cb-39e066ee97f0',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-EmployeeEditor-InWindow': {
            'editor': 'EmployeeEditor',
            'editorAlias': 'rms-employeeeditor',
            'editorUid': 'f6f75a72-b8a3-41f4-a7cb-39e066ee97f0',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-employeelist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-EmployeeEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-employeelist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Position_Company_Id'])) {
            this.hideColumnByDataIndex('Position_Company_Name', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Position_Department_Id'])) {
            this.hideColumnByDataIndex('Position_Department_Name', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Position_Id'])) {
            this.hideColumnByDataIndex('Position_Name', true, view);
            this.hideColumnByDataIndex('Position_Company_Name', true, view);
            this.hideColumnByDataIndex('Position_Department_Name', true, view);
        }
    },
});