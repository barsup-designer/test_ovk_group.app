Ext.define('B4.controller.DepartmentList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.DepartmentList',
        'B4.model.DepartmentListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-departmentlist',
    viewDataName: 'DepartmentList',
    // набор описаний действий реестра
    actions: {
        'Addition-DepartmentEditor-InWindow': {
            'editor': 'DepartmentEditor',
            'editorAlias': 'rms-departmenteditor',
            'editorUid': '23002cfc-5ad4-410f-820e-dcb97aad03ff',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-DepartmentEditor-InWindow': {
            'editor': 'DepartmentEditor',
            'editorAlias': 'rms-departmenteditor',
            'editorUid': '23002cfc-5ad4-410f-820e-dcb97aad03ff',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-departmentlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-DepartmentEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-departmentlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});