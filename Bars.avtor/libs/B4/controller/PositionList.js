Ext.define('B4.controller.PositionList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.PositionList',
        'B4.model.PositionListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-positionlist',
    viewDataName: 'PositionList',
    // набор описаний действий реестра
    actions: {
        'Addition-PositionEditor-InWindow': {
            'editor': 'PositionEditor',
            'editorAlias': 'rms-positioneditor',
            'editorUid': '893a591a-a3f7-41e1-9e4b-56cc97f6ee3b',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-PositionEditor-InWindow': {
            'editor': 'PositionEditor',
            'editorAlias': 'rms-positioneditor',
            'editorUid': '893a591a-a3f7-41e1-9e4b-56cc97f6ee3b',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-positionlist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-PositionEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-positionlist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
        if (!Ext.isEmpty(ctxParams['Department_Id'])) {
            this.hideColumnByDataIndex('Department_Name', true, view);
        }
        if (!Ext.isEmpty(ctxParams['Company_Id'])) {
            this.hideColumnByDataIndex('Company_Name', true, view);
        }
    },
});