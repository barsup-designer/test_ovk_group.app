Ext.define('B4.controller.CompanyList', {
    extend: 'B4.base.registry.Controller',
    requires: [
        'B4.view.CompanyList',
        'B4.model.CompanyListModel'],
    // псевдоним класса реестра
    viewAlias: 'rms-companylist',
    viewDataName: 'CompanyList',
    // набор описаний действий реестра
    actions: {
        'Addition-CompanyEditor-InWindow': {
            'editor': 'CompanyEditor',
            'editorAlias': 'rms-companyeditor',
            'editorUid': 'bf1503ab-249b-4f3d-800b-65290d25f1da',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        },
        'Editing-CompanyEditor-InWindow': {
            'editor': 'CompanyEditor',
            'editorAlias': 'rms-companyeditor',
            'editorUid': 'bf1503ab-249b-4f3d-800b-65290d25f1da',
            'hideToolbar': false,
            'maximizable': true,
            'mode': 'InWindow',
            'size': {
                'height': 500,
                'width': 500
            }
        }
    },
    aspects: [],
    init: function() {
        var me = this;
        me.control({
            scope: me,
            'rms-companylist': {
                'itemdblclick': function(sender, record) {
                    if (!sender.panel || sender.panel.isDestroyed == true) return;
                    this.defaultEditActionHandler(false, 'Editing-CompanyEditor-InWindow', sender.panel, record);
                }
            }
        });
        me.control({
            scope: me,
            'rms-companylist': {},
        });
        me.callParent(arguments);
    },
    applyCtxFilterParams: function(view, ctxParams) {
        var column = null;
    },
});