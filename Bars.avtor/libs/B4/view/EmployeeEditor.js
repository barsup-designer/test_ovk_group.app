Ext.define('B4.view.EmployeeEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-employeeeditor',
    title: 'Форма редактирования Сотрудник',
    rmsUid: 'f6f75a72-b8a3-41f4-a7cb-39e066ee97f0',
    requires: [
        'B4.form.PickerField',
        'B4.model.PositionEditorModel',
        'B4.model.PositionListModel',
        'B4.view.PositionList'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Employee_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'LastName',
        'modelProperty': 'Employee_LastName',
        'type': 'TextField'
    }, {
        'dataIndex': 'FirestName',
        'modelProperty': 'Employee_FirestName',
        'type': 'TextField'
    }, {
        'dataIndex': 'Patronymic',
        'modelProperty': 'Employee_Patronymic',
        'type': 'TextField'
    }, {
        'dataIndex': 'Position',
        'modelProperty': 'Employee_Position',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'f6f75a72-b8a3-41f4-a7cb-39e066ee97f0-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'EmployeeEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Фамилия',
                    'labelAlign': 'top',
                    'modelProperty': 'Employee_LastName',
                    'name': 'LastName',
                    'rmsUid': 'd4eeee7f-3e8d-47c5-8f71-6856b5f099ae',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '7b09fe65-7823-4a49-9aac-226bfacf7634',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '9f0ba765-c795-41b4-a1f8-4d3567d1137d',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Имя',
                    'labelAlign': 'top',
                    'modelProperty': 'Employee_FirestName',
                    'name': 'FirestName',
                    'rmsUid': '619c6335-fad8-40ef-a708-47abaa81cd0a',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'd6e800be-3963-4ccb-90e7-75cb23f034f3',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '38b89ac6-ddca-4673-a219-8b6d584e9c55',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Отчество',
                    'labelAlign': 'top',
                    'modelProperty': 'Employee_Patronymic',
                    'name': 'Patronymic',
                    'rmsUid': '38554103-d183-467e-92e2-d88c2ad698f9',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'bf180496-4ca9-4e4b-bd68-0063cc6a69ee',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '960ff822-fc6d-4653-84e4-4ffeb2777d4c',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'editable': false,
                    'fieldLabel': 'Должность',
                    'idProperty': 'Id',
                    'isContextAware': true,
                    'labelAlign': 'top',
                    'listView': 'B4.view.PositionList',
                    'listViewCtl': 'B4.controller.PositionList',
                    'model': 'B4.model.PositionListModel',
                    'modelProperty': 'Employee_Position',
                    'name': 'Position',
                    'rmsUid': '5f1743a9-0958-4ce4-a29a-1376b7b78c16',
                    'textProperty': 'Name',
                    'typeAhead': false,
                    'windowCfg': {
                        'border': false,
                        'height': 550,
                        'maximizable': true,
                        'title': 'Должность',
                        'width': 600
                    },
                    'xtype': 'b4pickerfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'd3ff96ec-2b14-4469-9daa-3e1f6eb75d8d',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '9648313b-7cfc-4ba0-a60b-f5b0e731955d',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Employee_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {} else {}
        return res;
    },
});