Ext.define('B4.view.PositionEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-positioneditor',
    title: 'Форма редактирования Должность',
    rmsUid: '893a591a-a3f7-41e1-9e4b-56cc97f6ee3b',
    requires: [
        'B4.form.PickerField',
        'B4.model.Company',
        'B4.model.CompanyEditorModel',
        'B4.model.Department',
        'B4.model.DepartmentEditorModel'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Position_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Name',
        'modelProperty': 'Position_Name',
        'type': 'TextField'
    }, {
        'dataIndex': 'Department',
        'modelProperty': 'Position_Department',
        'type': 'SelectorField'
    }, {
        'dataIndex': 'Company',
        'modelProperty': 'Position_Company',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '893a591a-a3f7-41e1-9e4b-56cc97f6ee3b-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'PositionEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Наименование',
                    'labelAlign': 'top',
                    'modelProperty': 'Position_Name',
                    'name': 'Name',
                    'rmsUid': '2c74d424-f220-4aa5-9a15-035cbf89b7df',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': 'c5e67cb8-2ead-40c0-b357-adee6278d98e',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'dc26897e-cfa7-48ed-95ac-f8739b3fcb4b',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [],
                'layout': 'anchor',
                'rmsUid': '3356ddf4-0a91-4326-a50f-b55ce21008c7',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'd7b89aa3-9829-4ebb-ba6c-4d3c9acbae22',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [],
                'layout': 'anchor',
                'rmsUid': 'e80501dd-bb89-4589-9f1c-502df647785e',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '4250d264-ac92-474b-b3f9-97d24b23d45c',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Position_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});