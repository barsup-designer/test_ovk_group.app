Ext.define('B4.view.EmployeeList', {
    'alias': 'widget.rms-employeelist',
    'dataSourceUid': '5f551863-5f51-43cc-b96c-ec1be7b2fe29',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.EmployeeListModel',
    'stateful': true,
    'title': 'Реестр Сотрудник',
    requires: [
        'B4.model.EmployeeListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': 'a2c79f78-f87a-45b7-a2e0-a8a3f6f989d2',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-EmployeeEditor-InWindow',
        'rmsUid': 'da42d98c-66f2-4580-8df0-349d3bc508a9',
        'text': 'Добавить'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-EmployeeEditor-InWindow',
        'rmsUid': '2e839610-58c4-45f0-a928-7b2de37901e6',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': 'd8a6e57b-ccb7-4064-bc93-392cfb05e60c',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'LastName',
                'dataIndexAbsoluteUid': 'FieldPath://5f551863-5f51-43cc-b96c-ec1be7b2fe29$dc221e1a-d04e-4a13-8e8d-1351f3c410f8',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '71d45fa8-20a7-4241-9b21-f2704a14cf85',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Фамилия',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'FirestName',
                'dataIndexAbsoluteUid': 'FieldPath://5f551863-5f51-43cc-b96c-ec1be7b2fe29$f6c88cf8-295c-40a2-a84c-d843c90709fd',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '483d7fd2-17a9-478a-b328-e618f179a69b',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Имя',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Patronymic',
                'dataIndexAbsoluteUid': 'FieldPath://5f551863-5f51-43cc-b96c-ec1be7b2fe29$db263b66-54f0-4f50-a18c-06ff2c9537e1',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '3c378657-ea89-4a54-8dc3-34d0d24e6410',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Отчество',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Position_Company_Name',
                'dataIndexAbsoluteUid': 'FieldPath://5f551863-5f51-43cc-b96c-ec1be7b2fe29$61d6fcac-6154-4abc-8368-efd58f5b0028$d4f8e7e4-d53a-4fbd-b4fa-ce8b45a0493f$65b673f9-f5b7-43b1-9d39-b16ec0fb507c',
                'dataIndexRelativePath': 'Position.Company.Name',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '8a84f546-9587-4449-9862-7ec2344b6db6',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Компания',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Position_Department_Name',
                'dataIndexAbsoluteUid': 'FieldPath://5f551863-5f51-43cc-b96c-ec1be7b2fe29$61d6fcac-6154-4abc-8368-efd58f5b0028$bba51e9c-4ebe-47bc-880f-1b9341574647$42a3dcb7-a110-46e4-ae0d-d91d43c81119',
                'dataIndexRelativePath': 'Position.Department.Name',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'a376395b-455f-4870-8375-0a691fd1f3b5',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Отдел',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Position_Name',
                'dataIndexAbsoluteUid': 'FieldPath://5f551863-5f51-43cc-b96c-ec1be7b2fe29$61d6fcac-6154-4abc-8368-efd58f5b0028$fa415c7b-cd95-43f6-8891-6ddab9e530f8',
                'dataIndexRelativePath': 'Position.Name',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '276419cd-9fa2-407f-8432-4bac9be1ca16',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Должность',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});