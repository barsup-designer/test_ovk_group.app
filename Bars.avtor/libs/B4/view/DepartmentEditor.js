Ext.define('B4.view.DepartmentEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-departmenteditor',
    title: 'Форма редактирования Отдел',
    rmsUid: '23002cfc-5ad4-410f-820e-dcb97aad03ff',
    requires: [
        'B4.form.PickerField',
        'B4.model.Company',
        'B4.model.CompanyEditorModel'],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Department_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Name',
        'modelProperty': 'Department_Name',
        'type': 'TextField'
    }, {
        'dataIndex': 'Company',
        'modelProperty': 'Department_Company',
        'type': 'SelectorField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': '23002cfc-5ad4-410f-820e-dcb97aad03ff-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'DepartmentEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': true,
                    'anchor': '100%',
                    'fieldLabel': 'Наименование',
                    'labelAlign': 'top',
                    'modelProperty': 'Department_Name',
                    'name': 'Name',
                    'rmsUid': '23dfdd70-4528-4d0f-9ffc-b88ec5be0eee',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '831c394f-2d5a-4f72-b137-508684c43abc',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': 'b94b28dc-d678-450e-9a5d-6ab6eafc37f4',
            'xtype': 'container'
        }, {
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [],
                'layout': 'anchor',
                'rmsUid': 'cc0a3b8b-ccb5-43cf-ac0b-5ad24f516787',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '8e42243d-8143-4c6d-ae60-af1a13a0f0aa',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Department_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});