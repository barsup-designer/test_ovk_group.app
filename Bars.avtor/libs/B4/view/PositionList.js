Ext.define('B4.view.PositionList', {
    'alias': 'widget.rms-positionlist',
    'dataSourceUid': '6fe4c6e0-2fdf-477a-bf72-8ba97ae13f0e',
    'enableColumnHide': true,
    'enableColumnMove': true,
    'enableColumnResize': true,
    'extend': 'B4.base.registry.View',
    'model': 'B4.model.PositionListModel',
    'stateful': true,
    'title': 'Реестр Должность',
    requires: [
        'B4.model.PositionListModel',
        'B4.ux.grid.plugin.QueryBuilderBar',
        'B4.ux.grid.plugin.StyleConditions',
        'Ext.ux.grid.FilterBar'],
    features: [{
        'ftype': 'grouping'
    }],
    tbar: [{
        'actionName': 'update',
        'hidden': false,
        'iconCls': 'icon-arrow-refresh',
        'itemId': 'Refresh',
        'rmsUid': '9ccb5427-9847-458d-925c-ddfa509f36ed',
        'text': 'Обновить'
    }, {
        'actionName': 'add',
        'hidden': false,
        'iconCls': 'icon-add',
        'itemId': 'Addition-PositionEditor-InWindow',
        'rmsUid': '98fab157-21f4-4f93-a583-54a392219072',
        'text': 'Добавить Должность'
    }, {
        'actionName': 'edit',
        'hidden': false,
        'iconCls': 'icon-pencil',
        'itemId': 'Editing-PositionEditor-InWindow',
        'rmsUid': '031be5d0-2647-4d72-ae4a-446bbcfb17cc',
        'text': 'Редактировать'
    }, {
        'actionName': 'delete',
        'hidden': false,
        'iconCls': 'icon-delete',
        'itemId': 'Deletion',
        'rmsUid': 'fc3f3279-da1a-45d3-a85f-647bc4b32968',
        'text': 'Удалить'
    }],
    lbar: [],
    rbar: [],
    bbar: [],
    paging: 50,
    initComponent: function() {
        var me = this;
        Ext.apply(me, {
            columns: [{
                'dataIndex': 'Name',
                'dataIndexAbsoluteUid': 'FieldPath://6fe4c6e0-2fdf-477a-bf72-8ba97ae13f0e$fa415c7b-cd95-43f6-8891-6ddab9e530f8',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': 'b241b741-5ac9-4af7-bf0d-8d2429ad8a20',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Наименование',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Department_Name',
                'dataIndexAbsoluteUid': 'FieldPath://6fe4c6e0-2fdf-477a-bf72-8ba97ae13f0e$bba51e9c-4ebe-47bc-880f-1b9341574647$42a3dcb7-a110-46e4-ae0d-d91d43c81119',
                'dataIndexRelativePath': 'Department.Name',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '6ff204aa-75a2-4ea5-9f9a-4cdb5861e484',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Отдел',
                'xtype': 'gridcolumn'
            }, {
                'dataIndex': 'Company_Name',
                'dataIndexAbsoluteUid': 'FieldPath://6fe4c6e0-2fdf-477a-bf72-8ba97ae13f0e$d4f8e7e4-d53a-4fbd-b4fa-ce8b45a0493f$65b673f9-f5b7-43b1-9d39-b16ec0fb507c',
                'dataIndexRelativePath': 'Company.Name',
                'decimalPrecision': 2,
                'filter': {
                    'xtype': 'b4-filter-field-list-of-string-property'
                },
                'flex': 1,
                'menuDisabled': false,
                'multisortable': false,
                'rmsUid': '16d436a1-c33e-496f-87bc-1923fe189660',
                'sortable': true,
                'summaryRenderer': function(value) {
                    return Ext.String.format('{0}', value);
                },
                'summaryType': 'none',
                'text': 'Компания',
                'xtype': 'gridcolumn'
            }],
            plugins: [{
                'ptype': 'filterbar',
                'renderHidden': false,
                'showClearAllButton': false,
                'showShowHideButton': false
            }, {
                'conditions': [],
                'ptype': 'gridStyleConditions'
            }, {
                'collapsed': true,
                'dockPosition': 'left',
                'ptype': 'gridQueryBuilderBar',
                'queries': []
            }]
        });
        me.callParent(arguments);
    }
});