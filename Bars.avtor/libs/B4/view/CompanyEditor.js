Ext.define('B4.view.CompanyEditor', {
    extend: 'B4.base.form.View',
    alias: 'widget.rms-companyeditor',
    title: 'Форма редактирования Компания',
    rmsUid: 'bf1503ab-249b-4f3d-800b-65290d25f1da',
    requires: [],
    metaFields: [{
        'dataIndex': 'Id',
        'modelProperty': 'Company_Id',
        'type': 'hidden'
    }, {
        'dataIndex': 'Name',
        'modelProperty': 'Company_Name',
        'type': 'TextField'
    }],
    dockedItems: [{
        'dock': 'top',
        'items': [{
            'actionName': 'save',
            'iconCls': 'icon-disk',
            'menu': [{
                'actionName': 'saveclose',
                'iconCls': 'icon-disk-black',
                'text': 'Сохранить и закрыть'
            }],
            'text': 'Сохранить',
            'xtype': 'splitbutton'
        }, {
            'xtype': 'tbfill'
        }, {
            'xtype': 'b4closebutton'
        }],
        'rmsUid': 'bf1503ab-249b-4f3d-800b-65290d25f1da-Form-TopToolBar',
        'xtype': 'toolbar'
    }],
    items: [{
        xtype: 'container',
        itemId: 'CompanyEditor-container',
        'layout': {
            'type': 'anchor'
        },
        items: [{
            'anchor': '100%',
            'dockedItems': [],
            'items': [{
                'defaults': {
                    'margin': '5 5 5 5'
                },
                'dockedItems': [],
                'flex': 1,
                'items': [{
                    'allowBlank': false,
                    'anchor': '100%',
                    'fieldLabel': 'Наименование',
                    'labelAlign': 'top',
                    'modelProperty': 'Company_Name',
                    'name': 'Name',
                    'rmsUid': 'ff4ae605-cf48-4e0f-9d6b-4af1510b1f16',
                    'xtype': 'textfield'
                }],
                'layout': 'anchor',
                'rmsUid': '9f67eb85-9364-4e54-9543-605fbeb1b8a7',
                'xtype': 'container'
            }],
            'layout': 'hbox',
            'rmsUid': '51e882df-ba0b-45fc-a45f-806b86927cf8',
            'xtype': 'container'
        }, {
            'hidden': true,
            'items': [{
                'modelProperty': 'Company_Id',
                'name': 'Id',
                'xtype': 'hidden'
            }],
            'xtype': 'panel'
        }]
    }],
    initComponent: function() {
        var me = this;
        me.callParent(arguments);
        me.submitTimeout = 20;
    },
    loadRecord: function(rec) {
        var me = this,
            res = me.callParent(arguments),
            entityId = rec.get('Id');
        if (Ext.isNumber(entityId) && entityId > 0) {
            me.setTitle(rec.get('Name'));
        } else {}
        return res;
    },
});