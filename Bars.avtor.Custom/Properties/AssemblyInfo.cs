using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using Bars.B4.Utils;
using Bars.B4.Modules.Ecm7.Framework;

[assembly: AssemblyTitle("Bars.avtor.Custom")]
[assembly: AssemblyDescription("Зарплатный Профиль.Разработка (Кастомная часть для модуля avtor)")]
[assembly: AssemblyCompany("ЗАО \"БАРС Груп\"")]
[assembly: AssemblyProduct("Bars.avtor.Custom")]
[assembly: AssemblyCopyright("Copyright © 2018")]
[assembly: ComVisible(false)]
// формат версии ВерсияГенератора.Год.МесяцДень.КоличествоКомитов
[assembly: AssemblyVersion("2.2018.0702.30")]
// идентификатор модуля для миграций
[assembly: MigrationModule("Bars.avtor.Custom")]