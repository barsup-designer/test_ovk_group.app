namespace Bars.B4.WebHost
{
    public class RmsApplicationInfo : IApplicationInfo
    {
        public string AppId => "test_ovk_group";
        public string Title => "Тестирование на примере реализации зарплатного профиля сотрудников ОВК Груп";
    }
}