using Bars.B4.WebHost.Controller;
using Bars.B4.Windsor;
using System.Collections.Generic;

namespace Bars.B4.WebHost
{
    /// <summary>
    /// Класс подключения модуля
    /// </summary>
    public partial class Module : AssemblyDefinedModule
    {
        public override IEnumerable<string> DependsOn => new[]{"Bars.B4.Modules.EsiaOauth"};
        /// <summary>
        /// Загрузка модуля
        /// </summary>
        public override void Install()
        {
            Container.ReplaceController<RmsLoginController>("login");
        }
    }
}